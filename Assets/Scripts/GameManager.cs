﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    static public int gameScore;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                Init();

                if (instance == null)
                {
                    Debug.LogError("You need a Game Manager");
                }
            }
            return instance;
        }
    }

    static void Init()
    {
        if (instance == null)
        {
            instance = FindObjectOfType(typeof(GameManager)) as GameManager;
        }
    }

    void Awake()
    {
        Init();
    }

    static public void IncrementScore()
    {
        gameScore += 5;
    }
}
