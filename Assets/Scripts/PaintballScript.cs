﻿using UnityEngine;
using System.Collections;

public class PaintballScript : MonoBehaviour 
{
    public GameObject paintballSplatPrefab;

    void OnCollisionEnter(Collision collision)
    {
        // if both layers are remote, don't splat on themselves
        if (!(gameObject.layer == LayerMask.NameToLayer("RemotePlayer") && collision.gameObject.layer == LayerMask.NameToLayer("RemotePlayer")) &&
                   collision.gameObject.layer != LayerMask.NameToLayer("LocalPlayer"))
        {
            GameObject paintballSplat = (GameObject)Instantiate(paintballSplatPrefab, transform.position, Quaternion.Euler(collision.gameObject.transform.forward));
            Destroy(paintballSplat, 2.0f);
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (transform.position.y < -3.0f)
        {
            Debug.Log("posistion low");
            Destroy(gameObject);
        }
    }
}