﻿using UnityEngine;
using System.Collections;

public class PlayerHit : MonoBehaviour
{
    private BroadcastPlayerHit broadcastPlayerHit;

    void Awake()
    {
        broadcastPlayerHit = transform.root.GetComponent<BroadcastPlayerHit>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("RemotePlayer"))
        {
            broadcastPlayerHit.CmdRegisterPlayerHit();
        }
    }
}
