﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BroadcastPlayerHit : NetworkBehaviour
{
    private string ID;
    public GameObject paintballPrefab;

	// Use this for initialization
	void Awake ()
    {
        ID = GetComponent<NetworkIdentity>().netId.ToString();
	}

    [Command]
    public void CmdRegisterPlayerHit()
    {
        Debug.Log(ID + " player was just hit.");
    }

    [Command]
    public void CmdMakeRemotePaintball(Vector3 pos, Quaternion rot, float velocity)
    {
        if (!isLocalPlayer)
        {
            GameObject paintball = (GameObject)Instantiate(paintballPrefab, pos, rot);
            paintball.GetComponent<Rigidbody>().AddForce(paintball.transform.forward * velocity);
            paintball.layer = LayerMask.NameToLayer("RemotePlayer");
        }
    }
}
