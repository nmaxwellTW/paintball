﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class PlayerHealthTest {

	[Test]
	public void TestStartsWithDefaultHealthTest()
	{
        var playerHealth = new PlayerHealth();

		Assert.AreEqual(100, playerHealth.Health);
	}

    [Test]
    public void TestIncrementsHealthOnCollisionWithHealthPowerup()
    {
        var playerHealth = new PlayerHealth();

        // collide with +10 powerup

        Assert.AreEqual(110, playerHealth.Health);
    }
}
